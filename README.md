# Scheme Interpreter

This is a learning project to implement an interpreter for the Scheme Lisp dialect,
as defined and used in the "Structure and Interpretation of Computer Programs" (SICP) book.

> It doesn't matter much what the programs are about or what applications they serve. 
What does matter is how well they perform and how smoothly they fit with other programs 
in the creation of still greater programs. 
The programmer must seek both perfection of part and adequacy of collection. 

> <...> programs must be written for people to read, 
and only incidentally for machines to execute.

> <...> "computer science" is not a science and <...> its significance 
has little to do with computers. 
The computer revolution is a revolution in the way we think 
and in the way we express what we think.

> Well-designed computational systems, like well-designed automobiles or nuclear reactors, 
are designed in a modular manner, so that the parts can be constructed, replaced, and debugged separately.
