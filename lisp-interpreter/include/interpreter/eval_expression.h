#ifndef LISP_INTERPRETER_EVAL_EXPRESSION_H
#define LISP_INTERPRETER_EVAL_EXPRESSION_H

#include <string>
#include <variant>

class EvaluatedExpression {
private:
    std::variant<double, std::string> value;
public:
    explicit EvaluatedExpression(double num) : value {num} {}
    explicit EvaluatedExpression(std::string s) : value {std::move(s)} {}

    [[nodiscard]]
    bool is_number() const {
        return std::holds_alternative<double>(value);
    }

    [[nodiscard]]
    double get_number() const {
        return std::get<double>(value);
    }

    [[nodiscard]]
    bool is_symbol() const {
        return std::holds_alternative<std::string>(value);
    }

    [[nodiscard]]
    std::string get_symbol() const {
        return std::get<std::string>(value);
    }

    [[nodiscard]]
    std::string to_string() const {
        if (is_number()) {
            return std::to_string(get_number());
        } else {
            return get_symbol();
        }
    }
};

#endif
