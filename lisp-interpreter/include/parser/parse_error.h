#ifndef LISP_INTERPRETER_PARSE_ERROR_H
#define LISP_INTERPRETER_PARSE_ERROR_H

#include <stdexcept>
#include <string>

enum class ParseErrorType {
    EmptyExpression,
    PrematureEndOfExpression,
    TokenOutsideExpression,
    UnmatchedClosedParenthesis
};

struct parse_error : public std::runtime_error {

    explicit parse_error(ParseErrorType err_type) :
        runtime_error(get_description(err_type)) {}

    [[nodiscard]]
    static const char* get_description(ParseErrorType errorType) {
        switch (errorType) {
            case ParseErrorType::EmptyExpression:
                return "Empty expression";
            case ParseErrorType::PrematureEndOfExpression:
                return "Input ended before expression is closed";
            case ParseErrorType::TokenOutsideExpression:
                return "Found a token outside of any expression";
            case ParseErrorType::UnmatchedClosedParenthesis:
                return "Found unmatched closed parenthesis";
            default:
                return "(no details are provided for this error type)";
        }
    }
};

#endif
