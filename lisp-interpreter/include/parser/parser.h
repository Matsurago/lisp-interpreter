#ifndef LISP_INTERPRETER_PARSE_H
#define LISP_INTERPRETER_PARSE_H

#include "expression.h"
#include "parse_error.h"
#include "tokenizer.h"

class Parser {
private:
    [[nodiscard]]
    Expression parse(const std::vector<std::string> &tokens) const;
public:
    [[nodiscard]]
    Expression parse(const std::string &string) const {
        return parse(tokenize(string));
    }

};

#endif
