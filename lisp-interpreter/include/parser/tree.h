#ifndef LISP_INTERPRETER_TREE_H
#define LISP_INTERPRETER_TREE_H

#include <functional>
#include <vector>

// A classic Tree (Node) structure.
template <class T>
class Tree {
private:
    T value_ {};
    std::vector<Tree> children_;
public:
    // constructors

    Tree() = default;

    explicit Tree(const T& val) : value_(val) {}

    Tree(std::initializer_list<Tree> nodes) : children_ {nodes} {}

    // getters

    T value() const {
        return value_;
    }

    const std::vector<Tree>& children() const {
        return children_;
    }

    // setters (add only)

    void add_child(const Tree &node) {
        children_.push_back(node);
    }

    void add_children(std::initializer_list<Tree> nodes) {
        children_.insert(children_.end(), nodes.begin(), nodes.end());
    }

    // properties

    [[nodiscard]]
    bool is_leaf() const {
        return children_.empty();
    }

    // functions

    template <class V>
    V dfs_fold(const std::function<V(const V& acc, const Tree& node)>& f,
               const V& default_value) const {
        V result = default_value;
        return dfs_fold(this, f, result);
    }

private:
    template <class V>
    V dfs_fold(const Tree* root,
               const std::function<V(const V& acc, const Tree& node)>& f,
               V& acc) const {
        acc = f(acc, *root);

        for (const auto& node : root->children_) {
            acc = dfs_fold(&node, f, acc);
        }

        return acc;
    }
};

#endif
