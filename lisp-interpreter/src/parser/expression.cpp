#include <sstream>
#include "parser/expression.h"

std::string to_string(const Expression &expression) {
    if (expression.is_leaf()) {
        return expression.value();
    }

    std::stringstream result;
    result << "Expr< ";

    for (const auto& child : expression.children()) {
        result << to_string(child) << " ";
    }

    result << ">";
    return result.str();
}
