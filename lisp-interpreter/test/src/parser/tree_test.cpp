#include <gmock/gmock.h>
#include "parser/tree.h"

using namespace ::testing;

struct TreeTest : public Test {
    // used to test dfs_fold
    static std::string to_string(const std::string& acc, const Tree<int>& node) {
        return acc + std::to_string(node.value()) + " ";
    }
};

TEST_F(TreeTest, TestCreateEmpty) {
    Tree<int> tree;
    EXPECT_THAT(tree.value(), Eq(0));
    EXPECT_TRUE(tree.children().empty());
    EXPECT_TRUE(tree.is_leaf());
}

TEST_F(TreeTest, TestCreateSingleNode) {
    Tree<int> tree {3};
    EXPECT_THAT(tree.value(), Eq(3));
    EXPECT_TRUE(tree.is_leaf());
}

TEST_F(TreeTest, TestCreateSingleLayer) {
    Tree<int> tree;
    tree.add_child(Tree<int> {3});
    tree.add_child(Tree<int> {4});

    EXPECT_FALSE(tree.is_leaf());

    auto children = tree.children();
    EXPECT_THAT(children[0].value(), Eq(3));
    EXPECT_THAT(children[1].value(), Eq(4));
}

TEST_F(TreeTest, TestCreateTwoLayers) {
    Tree<int> first_node;
    first_node.add_child(Tree<int> {2});
    first_node.add_child(Tree<int> {5});

    Tree<int> tree;
    tree.add_child(first_node);

    EXPECT_FALSE(tree.is_leaf());

    auto children = tree.children();
    auto first_child = children[0];

    EXPECT_FALSE(first_child.is_leaf());

    auto grandchildren = first_child.children();
    auto first_grandchild = grandchildren[0];
    auto second_grandchild = grandchildren[1];

    EXPECT_TRUE(first_grandchild.is_leaf());
    EXPECT_THAT(first_grandchild.value(), Eq(2));
    EXPECT_TRUE(second_grandchild.is_leaf());
    EXPECT_THAT(second_grandchild.value(), Eq(5));
}

TEST_F(TreeTest, TestDFS) {
    Tree<int> first_node {4};
    first_node.add_children({Tree {2}, Tree {5}});

    Tree<int> second_node {3};
    second_node.add_children({Tree {8}, Tree {7}});

    Tree<int> tree {9};
    tree.add_children({first_node, second_node});

    auto result = tree.dfs_fold<std::string>(to_string, {});

    EXPECT_THAT(result, Eq("9 4 2 5 3 8 7 "));
}

TEST_F(TreeTest, TestDFSSingleNode) {
    Tree<int> tree {4};

    auto result = tree.dfs_fold<std::string>(to_string, {});

    EXPECT_THAT(result, Eq("4 "));
}
